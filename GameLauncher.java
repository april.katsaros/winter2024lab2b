import java.util.Scanner;

public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello! Would you like to play Hangman or Wordle? (Enter 1 for Hangman or 2 for Wordle)");
		int gameChoice = reader.nextInt();
		
		if (gameChoice == 1){
			runHangman();
		}
		else if (gameChoice == 2){
			runWordle();
		}
		
	}
	// this method will run Hangman.java
	public static void runHangman(){
		Scanner reader = new Scanner(System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    Hangman.runGame(word);	
	}
	// this method will run Wordle.java
	public static void runWordle(){
		String answer = Wordle.generateWord();
		Wordle.runGame(answer);
	}
	
}